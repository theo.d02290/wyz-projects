# Base des projets

Dans ce repository vous retrouver l'essentiel pour configurer votre environnement de developpement et les projets.

### Les différents services fournis par docker
- Un service de mail [mailpit]
- Un service d'emulation de AWS (S3, SQS etc...) [localstack]
- Un service local FTP [ftp]
- Un service traefik permettant l'accès au service docker via une adresse et une configuration HTTPS [traefik]


## Les différents service sont accessible depuis :

### - [MAILPIT](https://mailpit.traefik.me)
L'interface est accessible depuis [mailpit.traefik.me](https://mailpit.traefik.me)

Le port SMTP est le `1025`

Dans vos projets symfony si cela n'est pas déjà fait la variable d'environnement `MAILER_DSN` doit être défini avec la valeur suivante :
```
MAILER_DSN=smtp://localhost:1025
```

### - [LOCAL STACK]
Permet l'accès à un S3 et le système de queue SQS

Pour l'accès à un bucket S3 l'addresse est soit le nom du container, ou alors l'url tout dépend de ou vous utiliserais localstack [http://localstack.traefik.me](http://localstack.traefik.me)

L'access key et la secret key ne sont pas important, vous pouvez saisir n'importe qu'elle key elle seront valide.

### Attention
Si les buckets et/ou queue ne sont pas créer au démarrage de votre container, c'est que le script de démarrage n'as pas les bon droits.

Soit vous ne voyais aucun bucket dans cloudberry par exemple ou alors vous avez l'erreur suivante dans le container:
`PermissionError: [Errno 13] Permission denied: '/etc/localstack/init/ready.d/buckets.sh'`

Pour corriger ceci dans votre dossier de projets lancer la commande suivante :
```
chmod +x ./.docker/containers/localstack/config/buckets.sh
```
Puis relancer votre container localstack.

## - [FTP]
Un serveur FTP est mis à disposition pour réaliser vos tests en local sans passer par un FTP en ligne.

Son adresse est [https://ftp.traefik.me](https://ftp.traefik.me)
Le username et le mot de passe son : `alpineftp`

# Vous souhaiter rajouter un nouveau service ? Comment le rendre accessible via traefik ?

Si vous intégrer un nouveau service et que vous souhaiter l'exposer via un URL il faut simplement rajouter les networks et labels qu'il faut

Pour ce faire ajouter les 2 networks `service` et `traefik`

Puis rajouter les labels : 
```
      - "traefik.enable=true"
      - "traefik.docker.network=traefik"
      - "traefik.http.routers.<service-name>.rule=Host(`<url>.traefik.me`)"
      - "traefik.http.routers.<service-name>.tls=true"
      - "traefik.http.services.<service-name>.loadbalancer.server.port=<port-exposer-par-le-service>"
```

Il vous suffit juste de remplacer les balise <xxx> par la valeur souhaiter comme dans les service existant.


--- 

## A Savoir

Une fois que les containers sont UP vous pouvez les utiliser dans différents projet du moment qu'il partage le même network et si votre URL traefik doit être accessible entre container, penser à rajouter l'aliase dans le service traefik.

Il est néccesaire d'importer le certificat dans les container si l'HTTPS est utiliser vous les trouver sur le site suivant :
[CERTIFICATS](https://traefik.me/)